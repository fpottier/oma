(******************************************************************************)
(*                                                                            *)
(*                                    Oma                                     *)
(*                                                                            *)
(*                     Frédéric Bour, Inria Paris, Tarides                    *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the GNU Library General Public License version 2, with a         *)
(*  special exception on linking, as described in the file LICENSE.           *)
(*                                                                            *)
(******************************************************************************)

open Monolith

module R = Omaref
module C = Oma

(* -------------------------------------------------------------------------- *)

(* We have two abstract types, namely [region] and [point]. The type [point]
   is equipped with a [check] function. *)

let region =
  declare_abstract_type ()

let check _model =
  C.check, constant "check"

let point =
  declare_abstract_type ~check ()

(* Many operations require a valid point as an argument. *)

let valid =
  R.is_valid

(* [compare] requires two points that inhabit the same region. *)

let same_region c1 c2 =
  R.(same (region c1) (region c2))

(* We test [iter] by converting it to an [elements] function, which returns
   a list of points. *)

let elements iter r =
  let cs = ref [] in
  iter (fun c -> cs := c :: !cs) r;
  List.rev !cs

(* -------------------------------------------------------------------------- *)

(* Declare the operations. *)

let () =

  let spec = unit ^> point in
  declare "create" spec R.create C.create;

  (* When testing with a small value of [w], that is, a small capacity, the
     functions [after] and [before] can fail by exhausting the space of
     available tags. This is why we use the exceptional arrow combinator
     [^!>] instead of the ordinary arrow combinator [^>]. *)
  let spec = (valid % point) ^!> point in
  declare "after" spec R.after C.after;
  declare "before" spec R.before C.before;

  let spec =
    (valid % point) ^>> fun c1 ->
    (valid % (same_region c1 % point)) ^>
    int
  in
  declare "compare" spec R.compare C.compare;
    (* Comparing the results of [compare] in the candidate implementation
       and in the reference implementation makes sense because [compare]
       always returns -1, 0, or +1. *)

  let spec = point ^> region in
  declare "region" spec R.region C.region;

  let spec = region ^> region ^> bool in
  declare "same" spec R.same C.same;

  let spec = (valid % point) ^> unit in
  declare "invalidate" spec R.invalidate C.invalidate;

  let spec = point ^> bool in
  declare "is_valid" spec R.is_valid C.is_valid;

  let spec =
    (valid % point) ^>> fun c1 ->
    (valid % (same_region c1 % point)) ^>
    unit
  in
  declare "invalidate_open_interval" spec
    R.invalidate_open_interval C.invalidate_open_interval;
  declare "invalidate_semi_open_interval" spec
    R.invalidate_semi_open_interval C.invalidate_semi_open_interval;

  (* [Unsafe.tag] cannot be tested, because the reference implementation
     does not use the same tags. *)

  let spec = (valid % point) ^> option point in
  declare "Unsafe.prev" spec R.Unsafe.prev C.Unsafe.prev;
  declare "Unsafe.next" spec R.Unsafe.next C.Unsafe.next;

  let spec = region ^> option point in
  declare "Unsafe.first" spec R.Unsafe.first C.Unsafe.first;
  declare "Unsafe.last" spec R.Unsafe.last C.Unsafe.last;

  let spec = region ^> int in
  declare "Unsafe.cardinal" spec R.Unsafe.cardinal C.Unsafe.cardinal;

  let spec = region ^> list point in
  declare "elements Unsafe.iter" spec
    (elements R.Unsafe.iter) (elements C.Unsafe.iter);

  ()

(* -------------------------------------------------------------------------- *)

(* Start the engine! *)

(* We know that [Oma.capacity] is small (e.g., 16). We want to use an amount
   of fuel that is sufficient to reach the data structure's capacity, but we
   do not allow this amount to exceed 32. *)

let () =
  let fuel = min 32 (2 * Oma.capacity) in
  let prologue () =
    dprintf "          open Oma;;\n";
    dprintf "          let elements iter r = let cs = ref [] in iter (fun c -> cs := c :: !cs) r; List.rev !cs;;\n"
  in
  main ~prologue fuel
