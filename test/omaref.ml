(******************************************************************************)
(*                                                                            *)
(*                                    Oma                                     *)
(*                                                                            *)
(*                     Frédéric Bour, Inria Paris, Tarides                    *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the GNU Library General Public License version 2, with a         *)
(*  special exception on linking, as described in the file LICENSE.           *)
(*                                                                            *)
(******************************************************************************)

(* A simple, inefficient implementation of order maintenance. *)

(* This implementation serves as a reference implementation while testing. *)

(* A region is implemented as a (fixed-size) array of cells.
   Each cell stores a pointer to its region and its index in
   the array. Inserting a new cell into the array requires
   moving (updating) all cells above it. *)

type region = {
          storage  : cell array;
  mutable cardinal : int;
}

and cell = {
  mutable index    : int;
          region   : region;
}

let[@inline] prev c : cell =
  let { index; region } = c in
  region.storage.(index - 1)

let[@inline] next c : cell =
  let { index; region } = c in
  region.storage.(index + 1)

(* Using a hard limit on the number of cells allows us to avoid
   the need for an extensible array. Using the same capacity as
   the candidate implementation [Oma] allows both implementations
   to raise an exception exactly in the same situations. *)
let capacity =
  Oma.capacity

let create () =
  let dummy = {
    index  = -1;
    region = { storage = [||]; cardinal = 0 }
  } in
  let region = {
    storage  = Array.make capacity dummy;
    cardinal = 1
  } in
  let index = 0 in
  let c = { index; region } in
  region.storage.(0) <- c;
  c

let is_valid c =
  0 <= c.index

let check c =
  assert (0 <= c.index);
  assert (c.index < c.region.cardinal);
  assert (c.region.storage.(c.index) == c);
  ()

let compare c1 c2 =
  assert (is_valid c1 && is_valid c2);
  assert (c1.region == c2.region);
  check c1; check c2;
  Int.compare c1.index c2.index

let region c =
  c.region

let same r1 r2 =
  r1 == r2

let insert index region =
  let { storage; cardinal } = region in
  if cardinal = capacity then
    invalid_arg "Oma: capacity exceeded";
  for i = cardinal - 1 downto index do
    (* Copy slot [i] into slot [i+1]. *)
    (* Renumber this cell. *)
    let c = storage.(i) in
    assert (c.index = i);
    storage.(i+1) <- c;
    c.index <- i+1
  done;
  (* Slot [index] is now free. *)
  (* Create a new cell and store it there. *)
  let c = { index; region } in
  storage.(index) <- c;
  region.cardinal <- region.cardinal + 1;
  c

let before c =
  assert (is_valid c);
  check c;
  let { index; region } = c in
  insert index region

let after c =
  assert (is_valid c);
  check c;
  let { index; region } = c in
  insert (index+1) region

let invalidate c =
  assert (is_valid c);
  check c;
  let { index; region } = c in
  let { storage; cardinal } = region in
  for i = index to cardinal-2 do
    (* Copy slot [i+1] into slot [i]. *)
    (* Renumber this cell. *)
    let c = storage.(i+1) in
    assert (c.index = i+1);
    storage.(i) <- c;
    c.index <- i
  done;
  c.index <- -1;
  region.cardinal <- region.cardinal - 1;
  assert (not (is_valid c))

(* Invalidating points one by one is slow, but simple and safe. *)

let rec invalidate_semi_open_interval lo hi =
  if compare lo hi < 0 then begin
    let successor = next lo in
    invalidate lo;
    invalidate_semi_open_interval successor hi
  end

let invalidate_open_interval lo hi =
  if compare lo hi < 0 then
    invalidate_semi_open_interval (next lo) hi

module Unsafe = struct

  let cardinal r =
    r.cardinal

  let prev c : cell option =
    let { index; region } = c in
    assert (0 <= index && index < region.cardinal);
    if index = 0 then
      None
    else
      Some (prev c)

  let next c : cell option =
    let { index; region } = c in
    assert (0 <= index && index < region.cardinal);
    if index = region.cardinal - 1 then
      None
    else
      Some (next c)

  let first r : cell option =
    if r.cardinal = 0 then
      None
    else
      Some r.storage.(0)

  let last r : cell option =
    if r.cardinal = 0 then
      None
    else
      Some r.storage.(r.cardinal - 1)

  let iter f r =
    for i = 0 to r.cardinal - 1 do
      f r.storage.(i)
    done

end
