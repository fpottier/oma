(******************************************************************************)
(*                                                                            *)
(*                                    Oma                                     *)
(*                                                                            *)
(*                     Frédéric Bour, Inria Paris, Tarides                    *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the GNU Library General Public License version 2, with a         *)
(*  special exception on linking, as described in the file LICENSE.           *)
(*                                                                            *)
(******************************************************************************)

(* The flag [debug] is automatically set to [true] in development builds (so,
   debugging code is present while testing) and to [false] in release builds. *)

(* Every use of [assert] and [say] must be protected by [if debug], so it is
   erased in release builds. *)

open Profile

let debug =
  not release

(* The flag [verbose] is normally set to [false], but can be manually set to
   [true] while debugging. *)

let verbose =
  false

(* The function [say] behaves like [eprintf] when [verbose] is set and has no
   effect when [verbose] is unset. *)

open Printf

let[@inline] say format =
  if verbose then fprintf stderr format else ifprintf stderr format

(* -------------------------------------------------------------------------- *)

(* The data structure is a doubly-linked list: each cell carries pointers,
   named [prev] and [next], to its predecessor and successor in the list.
   The address of a cell is public: when a new cell is created, its address
   is returned to the client.

   Each cell carries an integer tag. Each tag is unique: two distinct cells
   cannot carry the same tag. Furthermore, the list is sorted: if [c1] and
   [c2] are two consecutive cells, then [c1.tag < c2.tag] holds. Thus, the
   tags allow determining, in constant time, the relative ordering of two
   cells in the list.

   When a new cell is created and inserted in the list, it may be necessary
   to renumber one or more adjacent cells. Thus, the [tag] field is mutable.

   At the left end of the data structure is a low sentinel cell, which is
   its own predecessor, and carries the tag [lo_sentinel_tag]. At the right
   end is a high sentinel cell, which is its own successor, and carries the
   tag [hi_sentinel_tag]. The existence of these sentinel cells simplifies
   the code in several ways.

   The sentinel cells are never exposed to the client. Thus, every public
   function that expects a cell as an argument can assume that this cell is
   not a sentinel cell.

   Upon request, a cell can be invalidated. It is then extracted out of the
   doubly-linked list, and is no longer part of the data structure. Its tag
   becomes available for use again. Invalidated cells are marked with the
   special tag [invalid_tag].

   With each doubly-linked list, we associate a unique record, the region,
   which identifies this data structure and counts the number of cells in
   this region. (The two sentinel cells are not counted. Invalidated cells
   are not counted). Every cell carries a pointer to its region. Conversely,
   every region carries a pointer to its low and high sentinel cells. This
   allows the public function [Unsafe.iter] to take a region as an argument,
   and allows implementing [Unsafe.first] and [Unsafe.last] with constant
   time complexity. *)

type region = {
  mutable cardinal : int;
  lo_sentinel  : cell;
  hi_sentinel  : cell;
}

and cell = {
  mutable tag  : int;
  mutable prev : cell;
  mutable next : cell;
  region       : region;
}

type point =
  cell

(* -------------------------------------------------------------------------- *)

(* [w] is the width of a tag in bits. *)

(* In development builds, [w] is set to a very small value, so as to
   facilitate testing. In release builds, it is set to a large value.
   We need [1 lsl w] to be positive, so the largest acceptable value
   is [Sys.word_size - 3], that is, 61 bits. This yields a capacity
   of about [2.10^18], that is, two billion billions. *)

(* In release builds, [w] is so large that it is impossible to exceed
   the capacity of the data structure. That would require too much
   time and space. *)

let w = if debug then 4 else Sys.word_size - 3

(* The capacity of the data structure is [1 lsl w], that is, two to
   the power of [w]. It is called [u] in the paper. *)

let (* public *) capacity = 1 lsl w

(* The regular tags are comprised between [min_tag] and [max_tag],
   inclusive. *)

let min_tag = 0
let max_tag = capacity - 1

(* The low sentinel tag is [min_tag - 1], that is, [-1].
   The high sentinel tag is [max_tag + 1], that is, [capacity]. *)

let lo_sentinel_tag = -1
let hi_sentinel_tag = capacity

(* The tag that is used to identify invalidated cells, [invalid_tag],
   is [-2]. It could be any non-regular, non-sentinel tag. *)

let invalid_tag = min_tag - 2

(* Sanity checks. *)

let () =
  if debug then assert (0 < max_tag);
  if debug then assert (max_tag < capacity);
  if debug then assert (lo_sentinel_tag = min_tag - 1);
  if debug then assert (hi_sentinel_tag = max_tag + 1);
  ()

(* -------------------------------------------------------------------------- *)

(* A regular tag is comprised between [min_tag] and [max_tag]. *)

let[@inline] is_regular_tag tag =
  min_tag <= tag && tag <= max_tag

(* The sentinel cells can be identified by their tag. *)

let[@inline] is_lo_sentinel c =
  c.tag = lo_sentinel_tag

let[@inline] is_hi_sentinel c =
  c.tag = hi_sentinel_tag

let[@inline] is_sentinel c =
  is_lo_sentinel c || is_hi_sentinel c

let[@inline] is_not_sentinel c =
  not (is_sentinel c)

(* The first and last cells are identified by the fact that they are
   preceded or followed by a sentinel cell. *)

let[@inline] is_first c =
  is_lo_sentinel c.prev

let[@inline] is_last c =
  is_hi_sentinel c.next

(* An invalidated cell is identified by its tag. *)

let[@inline] is_invalid c =
  c.tag = invalid_tag

let[@inline] (* public *) is_valid c =
  not (is_invalid c)

let[@inline] must_be_valid c =
  if is_invalid c then
    invalid_arg "Oma: invalidated point"

(* A cell that is passed to us by the client is certainly not a sentinel
   cell. Furthermore, it must not be a previously invalidated cell. If it
   is, then the caller is in error. Every public function, except [is_valid]
   and [check], should apply [sanitize] to its arguments. *)

let[@inline] sanitize c =
  if debug then assert (is_not_sentinel c);
  must_be_valid c

(* -------------------------------------------------------------------------- *)

(* Creation involves creating a fresh region, a fresh regular cell,
   and two fresh sentinel cells. *)

(* This may be the first time that I use [let rec] to create 4 records
   that point to each other and form a strongly connected component. *)

let (* public *) create () =
  let tag = capacity / 2 in
  if debug then assert (is_regular_tag tag);
  let rec region =
    { cardinal = 1; lo_sentinel; hi_sentinel }
  and lo_sentinel =
    { prev = lo_sentinel; next = c; tag = lo_sentinel_tag; region }
  and c =
    { prev = lo_sentinel; next = hi_sentinel; tag; region }
  and hi_sentinel =
    { prev = c; next = hi_sentinel; tag = hi_sentinel_tag; region }
  in c

(* -------------------------------------------------------------------------- *)

(* Testing whether two cells inhabit the same region is a matter of comparing
   the two regions using physical equality. *)

let[@inline] (* public *) region c =
  (* no need to sanitize here *)
  c.region

let[@inline] (* public *) same r1 r2 =
  r1 == r2

(* -------------------------------------------------------------------------- *)

(* Testing which way two cells (which inhabit the same region) are ordered
   is a matter of comparing the two integer tags. *)

(* We rely on the (undocumented) property that [Int.compare] always returns
   -1, 0, or +1. *)

let (* public *) compare c1 c2 =
  sanitize c1; sanitize c2;
  if not (same c1.region c2.region) then
    invalid_arg "Oma: points inhabit distinct regions";
  let code = Int.compare c1.tag c2.tag in
  if debug then assert (-1 <= code && code <= 1);
  code

(* -------------------------------------------------------------------------- *)

(* Invalidating a cell involves assigning it the tag [invalid_tag], removing
   it from the doubly-linked list, and decrementing the region's cardinal. *)

let (* public *) invalidate c =
  sanitize c;
  c.tag <- invalid_tag;
  let { prev; next; region; _ } = c in
  prev.next <- next;
  next.prev <- prev;
  c.next <- c;
  c.prev <- c;
  region.cardinal <- region.cardinal - 1;
  if debug then assert (not (is_valid c))

(* -------------------------------------------------------------------------- *)

(* Invalidating all of the points in an interval delimited by two points. *)

(* [invalidate_semi_open_interval count c hi] invalidates the semi-open
   interval comprised between [c] (included) and [hi] (excluded). It returns
   [count] plus the number of invalidated points. The points [c] and [hi] must
   be valid, inhabit the same region, and must satisfy [compare c hi <= 0]. *)

let rec invalidate_semi_open_interval count c hi : int =
  if debug then assert (compare c hi <= 0);
  if c == hi then
    count
  else begin
    let { next; _ } = c in
    c.tag <- invalid_tag;
    c.next <- c;
    c.prev <- c;
    invalidate_semi_open_interval (count + 1) next hi
  end

(* [invalidate_nonempty_open_interval lo hi] invalidates the open interval
   comprised between [lo] (excluded) and [hi] (excluded). The points [lo] and
   [hi] must be valid, inhabit the same region, and must satisfy [compare
   lo.next hi < 0], which means that the open interval contains at least one
   inhabitant, namely [lo.next]. *)

let invalidate_nonempty_open_interval lo hi =
  if debug then assert (compare lo.next hi < 0);
  let { next; region; _ } = lo in
  let count = invalidate_semi_open_interval 0 next hi in
  lo.next <- hi;
  hi.prev <- lo;
  region.cardinal <- region.cardinal - count

(* [invalidate_open_interval lo hi] invalidates the open interval comprised
   between [lo] (excluded) and [hi] (excluded). The points [lo] and [hi] must
   be valid and inhabit the same region; otherwise, [Invalid_argument _] is
   raised. The interval may be empty; in that case, nothing happens. *)

(* Invoking [compare] sanitizes the two points and checks that they inhabit
   the same region. The test [lo.next.tag < hi.tag] guarantees that the
   interval is nonempty; if it is empty, there is nothing to do. *)

let (* public *) invalidate_open_interval lo hi =
  if compare lo hi < 0 && lo.next.tag < hi.tag then
    invalidate_nonempty_open_interval lo hi

(* [invalidate_semi_open_interval lo hi] invalidates the semi-open interval
   comprised between [lo] (included) and [hi] (excluded). The points [lo] and
   [hi] must be valid and inhabit the same region; otherwise,
   [Invalid_argument _] is raised. The interval may be empty; in that case,
   nothing happens. *)

(* Invoking [compare] sanitizes the two points, checks that they inhabit the
   same region, and guarantees that the interval is nonempty. If it is empty,
   there is nothing to do. *)

let (* public *) invalidate_semi_open_interval lo hi =
  if compare lo hi < 0 then
    invalidate_nonempty_open_interval lo.prev hi

(* -------------------------------------------------------------------------- *)

(* This auxiliary function is used in an assertion. *)

let[@inline] is_power_of_two n =
  assert (n > 0);
  n land (n - 1) = 0

(* -------------------------------------------------------------------------- *)

(* [find_span c] is invoked when two consecutive cells, [c] and [c.next],
   carry the same tag.

   This function determines a range of cells that must be renumbered. It
   returns a 4-tuple of:

   - [left],  the first cell that must be renumbered;
   - [low],   the first tag that is available for use while renumbering;
   - [range], the number of tags that are available for use,
              so the last available tag is [low+range-1];
              [range] is always a power of two;
   - [count], the number of cells that must be renumbered;
              the inequality [count <= range] holds. *)

let find_span c =

  (* The cells [c] and [c.next] are both valid, and cannot be sentinels. *)
  if debug then assert (is_not_sentinel c);
  if debug then assert (is_valid c);
  if debug then assert (is_not_sentinel c.next);
  if debug then assert (is_valid c.next);
  (* The cells [c] and [c.next] carry the same tag;
     that is the reason why renumbering is required. *)
  if debug then assert (c.tag = c.next.tag);
  if debug then assert (is_regular_tag c.tag);

  (* The loop that follows successively tries larger and larger ranges of
     tags, beginning with a narrow range of width 1, containing just the tag
     [tag]. The low and high tags in this range, [!low] and [!high], are [tag]
     and [tag]. The number of cells in this range is 2. The leftmost and
     rightmost cells in this range are [c] and [c.next]. *)
  let tag   = c.tag in
  let range = ref 1
  and low   = ref tag
  and high  = ref tag
  and count = ref 2
  and left  = ref c
  and right = ref c.next in

  (* At each loop iteration, we measure the density of the current range,
     which is [!count /. !range], and we test whether it exceeds a certain
     threshold. If it does, then this range is too crowded; we move on to the
     next iteration with a range that is twice larger. Otherwise, this range
     has enough space; we return. *)
  (* When the size of the range is doubled, this is done in such a way that
     [tag] still inhabits the current range. In fact, the low and high
     boundaries of the current range are computed by respectively clearing
     and setting the low-order bits of [tag]. *)
  (* The threshold changes at each loop iteration; it is initially 1 and
     evolves in a geometric manner towards [n /. capacity], which is the global
     density. To achieve this, we precompute a certain rate and multiply
     [threshold] by [rate] at each iteration. Because the maximum number of
     iterations is [w], the desired rate is the [w]-th root of the global
     density. *)
  let n     = c.region.cardinal in
  let global_density = float n /. float capacity in
  if debug then assert (global_density <= 1.0);
  let rate  = global_density ** (1.0 /. float w) in
  if debug then assert (rate <= 1.0);
  let threshold = ref 1.0 in

  (* The main [while] loop is guarded by a combination of three conditions:
     [!range < capacity], [!count > !range], and [density > !threshold]. *)

  (* This combination is of the form [A && (B || C)]. *)

  (* Intuitively, this condition is equivalent to [C] alone. Here is why.
     This argument is INFORMAL, as it ignores floating-point errors. *)

  (* First, [B] implies [C]. Indeed, [!count > !range] implies
     [density > 1], which itself implies [density > !threshold]. *)

  (* So, [A && (B || C)] is equivalent to [A && C]. *)

  (* Second, [¬A] implies [¬C]. Indeed, assume that [A] is false, i.e.,
     [!range = capacity] holds. Then, the loop must have run [w] times, so
     we have [!count = n] and [density = n /. capacity = !threshold]. So,
     the condition [density > !threshold] is false. *)

  (* So, [C] implies [A]. *)

  (* So, [A && C] is equivalent to [C]. *)

  (* This informal argument ignores floating-point errors, so it CANNOT be
     relied upon. This is why we use the condition [A && (B || C)]. *)

  (* When the loop ends, this condition is false, so we have
     [¬A] or [¬B && ¬C]. A fortiori, we have [¬A] or [¬B].
     However, [B] implies [A]
       (the maximal range is never overcrowded)
     so [¬A] implies [¬B]
     so [¬A || ¬B] is equivalent to [¬B].
     In other words, when the loop ends, we have [¬B], that is,
     [!count <= !range].
     This guarantees that renumbering this range is possible. *)

  (* It may seem as though the condition [A] is unnecessary and the loop
     could be guarded by just [B || C]. This is not the case. The presence
     of the condition [A] ensures that [!range <= capacity] is a loop
     invariant. Without this condition, due to floating-point errors,
     condition [C] could be true of the maximal range, and we would then
     incorrectly increase the range beyond [capacity]. *)

  while
    !range < capacity &&
    (
      !count > !range ||
      let density = float !count /. float !range in
      density > !threshold
    )
  do

    (* Debugging messages; visible only when [verbose] is set. *)
    if debug then begin
      let density = float !count /. float !range in
      say "!range = %d, !count = %d, density = %f, !threshold = %f\n"
        !range !count density !threshold;
      say "condition !count > !range is %b\n"
        (!count > !range);
      say "condition density > !threshold is %b\n%!"
        (density > !threshold)
    end;

    (* The following assertions form a loop invariant. *)

    (* The range is a power of two. *)
    if debug then assert (is_power_of_two !range);
    (* The current range is always at most the maximal range. *)
    if debug then assert (!range <= capacity);
    (* If this range is overcrowded then it cannot be the maximal range.
       In other words, [B] implies [A]. *)
    if debug then assert ((!count > !range) <= (!range < capacity));
    (* [!left] and [!right] are valid cells. *)
    if debug then assert (is_valid !left && is_valid !right);
    (* [!left] is below [!right]. *)
    if debug then assert (!left.tag <= !right.tag);
    (* [!low] is less than or equal to [!high]. *)
    if debug then assert (!low <= !high);
    (* The width of the closed interval [\[!low, !high\]] is [!range]. *)
    if debug then assert (!high - !low + 1 = !range);
    (* [!left] is the first cell whose tag is at least [!low]. *)
    if debug then assert (!left.prev.tag < !low);
    if debug then assert (!low <= !left.tag);
    (* [!right] is the last cell whose tag is at most [!high]. *)
    if debug then assert (!right.tag <= !high);
    if debug then assert (!high < !right.next.tag);
    (* [!left] lies to the left of the cells [c] and [c.next], while [!right]
       lies to their right. This implies that [tag] lies inside the closed
       interval [\[!low, !high\]]. *)
    if debug then assert (!left.tag <= tag);
    if debug then assert (tag <= !right.tag);
    (* In fact, [!low] and [!high] are determined by [tag] and by [!range],
       as follows. Let [mask] be the bit mask that corresponds to [!range].
       Then, [!low] is obtained from [tag] by setting these bits to zero,
       while [!high] is obtained from [tag] by setting these bits to one. *)
    let mask = !range - 1 in
    if debug then assert (!low = tag land (lnot mask));
    if debug then assert (!high = tag lor mask);

    (* Extend the current range, by doubling its size, either towards the left
       (by decreasing [!low]) or towards the right (by increasing [!high]), in
       such a way that is determined by [tag], as shown above. Then, adjust the
       [left] or [right] pointer, in linear time, by traversing the list. *)

    if tag land !range <> 0 then begin
      (* The bit is set in [tag] and [!low]. Clear it in [!low]. *)
      if debug then assert (!low land !range <> 0);
      low := !low lxor !range;
      while !left.prev.tag >= !low do
        if debug then assert (not (is_first !left));
        left := !left.prev;
        incr count
      done
    end
    else begin
      (* The bit is clear in [tag] and [!high]. Set it in [!high]. *)
      if debug then assert (!high land !range = 0);
      high := !high lxor !range;
      while !right.next.tag <= !high do
        if debug then assert (not (is_last !right));
        right := !right.next;
        incr count
      done
    end;
    range := !range lsl 1;

    (* Decrease the threshold for the next loop iteration. *)
    threshold := !threshold *. rate

  done;
  (* End of the main [while] loop. *)
  !left, !low, !range, !count
  (* End of [find_span]. *)

(* -------------------------------------------------------------------------- *)

(* [renumber_span left step tag count] renumbers [count] cells, beginning at
   the cell [left], beginning with the tag [tag], and incrementing this tag
   by [step] at each iteration. Both [step] and [count] must be at least 1. *)

let rec renumber_span left step tag count =
  (* Renumber this cell. *)
  if debug then say "Cell %d is renumbered %d\n%!" left.tag tag;
  left.tag <- tag;
  let count = count - 1 in
  if count = 0 then begin
    (* Upon reaching the end of the renumbered span, the ordering property
       should be preserved. Otherwise, [find_span] has messed up. *)
    if debug then assert (is_last left || tag < left.next.tag)
  end
  else begin
    if debug then assert (left.prev.tag < tag);
    renumber_span left.next step (tag + step) count
  end

(* [renumber c] is invoked when two consecutive cells, [c] and [c.next],
   carry the same tag. Using [find_span], it determines which cells should
   be renumbered; then, using [renumber_span], it renumbers these cells. *)

(* By computing [step = range / count], we attempt to leave a constant
   amount of free tag space in between every two consecutive cells. In the
   worst case, where the density of this range is high, [step] is 1, so all
   of the free tag space ends up at the right end of the range. One could
   instead try to place this free space between the cells [c] and [c.next],
   since this is the area where the client seems active. It is not clear
   whether this matters in practice, so we do not include this heuristic. *)

let renumber c =
  if debug then assert (is_not_sentinel c);
  if debug then assert (is_valid c);
  if debug then assert (c.tag = c.next.tag);
  let left, low, range, count = find_span c in
  if debug then say "Renumbering from %d to %d+%d; %d renumbered cells\n%!"
    low low range count;
  if debug then assert (count <= range);
  let step = range / count in
  if debug then assert (step >= 1);
  renumber_span left step low count

(* -------------------------------------------------------------------------- *)

(* We use [average] to pick a number within an interval. This function does
   not always round down; e.g., [average (-1) 0] is [0]. We DO exploit this
   property. When we call [average x y], we guarantee [lo_sentinel_tag <= x
   < y <= hi_sentinel_tag], where [lo_sentinel_tag] is [-1], and we expect
   the result to be a regular tag, that is, a tag comprised between
   [min_tag] and [max_tag], inclusive. We also expect the result to be
   comprised between [x] and [y], inclusive. *)

(* We do not actually need the result to be the average of [x] and [y]. This
   is just a heuristic. *)

let[@inline] average x y =
  (x land y) + (x lxor y) / 2

let[@inline] average x y =
  if debug then assert (lo_sentinel_tag <= x && x < y && y <= hi_sentinel_tag);
  let tag = average x y in
  if debug then assert (is_regular_tag tag);
  if debug then assert (x <= tag && tag <= y);
  tag

(* -------------------------------------------------------------------------- *)

(* The private function [after c] creates a new cell and inserts it between
   the cells [c] and [c.next]. If there is no space between these cells
   (that is, if these cells carry consecutive tags) then renumbering takes
   place. *)

(* We allow [c] to be the low sentinel cell. This lets us later implement
   [before] in terms of [after], thereby avoiding code duplication. *)

(* If the number of cells in the list is already equal to [capacity] then
   creating a new cell is impossible. In this case, [create] fails with
   an [Invalid_argument] exception. When [w] is sufficiently large, this
   situation can never arise. *)

let after c =
  (* [c] may be the low sentinel cell. *)
  if debug then assert (is_valid c);
  let { next; region; _ } = c in
  let n = region.cardinal in
  (* If [capacity] is reached, fail. *)
  if n = capacity then
    invalid_arg "Oma: capacity exceeded";
  (* Update the region's cardinal. *)
  region.cardinal <- n + 1;
  (* Compute a candidate tag for the new cell. *)
  let lo, hi = c.tag, c.next.tag in
  (* The new tag should be comprised between [lo] and [hi], both excluded.
     If there is no room between them, then renumbering is required. *)
  if debug then assert (lo < hi);
  let must_renumber = (hi - lo = 1) in
  (* If [lo] is [-1] and [hi] is [0] then [average] rounds up and produces
     [0], a valid tag; this is good. Similarly, if [lo] is [max_tag] and
     [hi] is [max_tag+1] then [average] rounds down and produces [max_tag],
     a valid tag. We do not assume that [average] always rounds down. *)
  let tag = average lo hi in
  if debug then assert (is_regular_tag tag);
  (* Create the new cell and insert it into the list. *)
  let c' = { prev = c; next; tag; region } in
  c.next <- c';
  next.prev <- c';
  (* If renumbering is required, do it. *)
  if must_renumber then begin
    (* The two cells that currently carry the same tag are either [c'] and
       its predecessor [c], or [c'] and its successor [next]. The sentinel
       cell is not among them, because [tag] is a valid tag. *)
    if debug then assert ((c.tag = tag) <> (tag = next.tag));
    renumber (if c.tag = tag then c else c')
  end;
  (* Done. *)
  if debug then say "New tag is %d\n%!" c'.tag;
  c'

(* Thanks to the presence of a low sentinel cell, the private function
   [before c] can be implemented in one line, as follows. *)

let[@inline] before c =
  after c.prev

(* -------------------------------------------------------------------------- *)

(* The public functions [after] and [before] add sanitization to the
   private functions [after] and [before] defined above. *)

let (* public *) after c =
  sanitize c;
  after c

let (* public *) before c =
  sanitize c;
  before c

(* -------------------------------------------------------------------------- *)

(* [check] is invoked by Monolith while testing. It performs a series of
   local runtime checks, in an attempt to detect violations of the data
   structure's invariants. *)

(* [check] must not assume that its argument is a valid cell. Indeed,
   Monolith applies [check] to all cells that it knows about. *)

(* In release builds, [debug] is false, so [check] has no effect. *)

let (* public *) check c =
  if debug then begin
    assert (is_not_sentinel c);
    if is_valid c then begin
      assert (is_valid c.prev);
      assert (is_valid c.next);
      assert (c.prev.next == c);
      assert (c.prev.tag < c.tag);
      assert (c.next.prev == c);
      assert (c.tag < c.next.tag)
    end
  end

(* -------------------------------------------------------------------------- *)

(* Unsafe operations. *)

module Unsafe = struct

  let (* public *) tag c =
    sanitize c;
    c.tag

  let (* public *) prev c =
    sanitize c;
    if is_first c then None else Some c.prev

  let (* public *) next c =
    sanitize c;
    if is_last c then None else Some c.next

  let (* public *) first r =
    if r.cardinal = 0 then None
    else Some r.lo_sentinel.next

  let (* public *) last r =
    if r.cardinal = 0 then None
    else Some r.hi_sentinel.prev

  let (* public *) cardinal r =
    r.cardinal

  let rec iter_from f c =
    if not (is_hi_sentinel c) then begin
      (* Read [c.next] before invoking [f]. *)
      let next = c.next in
      f c;
      iter_from f next
    end

  let (* public *) iter f r =
    let c = r.lo_sentinel.next in
    iter_from f c

end
