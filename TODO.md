## Testing

* It would be desirable to limit the number of calls to `create`, as we do
  not expect interesting test scenarios to arise out of multiple calls to
  `create`. This may require modifying Monolith.

## Specification

* Use a persistent assertion `x < y`.
