# ------------------------------------------------------------------------------

# The name of the library.
THIS     := oma

# The version number is automatically set to the current date,
# unless DATE is defined on the command line.
DATE     := $(shell /bin/date +%Y%m%d)

# This is used in [make release] to search CHANGES.md.
# Ideally, it should be derived from $(DATE).
DATE_WITH_SLASHES := $(shell /bin/date +%Y/%m/%d)

# The repository URL (https).
REPO     := https://gitlab.inria.fr/fpottier/$(THIS)

# The archive URL (https).
ARCHIVE  := $(REPO)/-/archive/$(DATE)/archive.tar.gz

# ------------------------------------------------------------------------------

# Compilation and cleanup.

# [make all] does NOT build the tests.

.PHONY: all
all:
	@ dune build @install

.PHONY: clean
clean:
	@ git clean -fdX

# [make test] requires Monolith.
#   opam install monolith

.PHONY: test
test:
	@ make -C test random

# ------------------------------------------------------------------------------

# Installation and uninstallation.

.PHONY: install
install: all
	@ dune install -p $(THIS)

.PHONY: uninstall
uninstall:
	@ ocamlfind remove $(THIS) || true

.PHONY: reinstall
reinstall: uninstall
	@ make install

# ------------------------------------------------------------------------------

# [make versions] compiles the library under many versions of OCaml,
# whose list is specified below.

# This requires appropriate opam switches to exist. A missing switch
# can be created like this:
#   opam switch create 4.03.0

VERSIONS := \
  4.12.0 \
  4.13.1 \
  4.14.1 \
  5.0.0  \
  5.1.0  \
  5.2.0  \

.PHONY: versions
versions:
	@(echo "(lang dune 3.8)" && \
	  for v in $(VERSIONS) ; do \
	    echo "(context (opam (switch $$v)))" ; \
	  done) > dune-workspace.versions
	@ dune build --workspace dune-workspace.versions -p $(THIS)

# ------------------------------------------------------------------------------

# [make show] shows the library's API.

.PHONY: show
show: all
	@ echo "#use \"topfind\";;\n#require \"oma\";;\n#show Oma;;" | dune exec ocaml

# ------------------------------------------------------------------------------

# Documentation.

# This requires odoc.
#   opam install odoc

DOCDIR = _build/default/_doc/_html
DOC    = $(DOCDIR)/index.html

.PHONY: doc
doc:
	@ rm -rf _build/default/_doc
	@ dune clean
	@ dune build @doc 2>&1 | ./doc.sh | sort | uniq
	@ echo "You can view the documentation by typing 'make view'".

.PHONY: view
view: doc
	@ echo Attempting to open $(DOC)...
	@ if command -v firefox > /dev/null ; then \
	  firefox $(DOC) ; \
	else \
	  open -a /Applications/Firefox.app/ $(DOC) ; \
	fi

.PHONY: export
export: doc
	ssh yquem.inria.fr rm -rf public_html/$(THIS)/doc
	scp -r $(DOCDIR) yquem.inria.fr:public_html/$(THIS)/doc

# ------------------------------------------------------------------------------

# Headers.

HEADACHE := headache
LIBHEAD  := $(shell pwd)/header.txt
FIND     := $(shell if command -v gfind >/dev/null ; then echo gfind ; else echo find ; fi)

.PHONY: headache
headache:
	@ for d in src test ; do \
	    $(FIND) $$d -regex ".*\.ml\(i\|y\|l\)?" \
	      -exec $(HEADACHE) -h $(LIBHEAD) "{}" ";" ; \
	  done

# ------------------------------------------------------------------------------

.PHONY: release
release:
# Make sure the current version can be compiled and installed.
	@ make uninstall
	@ make clean
	@ make install
# Check the current package description.
	@ opam lint
# Make sure a CHANGES entry with the current date seems to exist.
	@ if ! grep $(DATE_WITH_SLASHES) CHANGES.md ; then \
	    echo "Error: CHANGES.md has no entry with date $(DATE_WITH_SLASHES)." ; \
	    exit 1 ; \
	  fi
# Check if everything has been committed.
	@ if [ -n "$$(git status --porcelain)" ] ; then \
	    echo "Error: there remain uncommitted changes." ; \
	    git status ; \
	    exit 1 ; \
	  else \
	    echo "Now making a release..." ; \
	  fi
# Create a git tag.
	@ git tag -a $(DATE) -m "Release $(DATE)."
# Upload. (This automatically makes a .tar.gz archive available on gitlab.)
	@ git push
	@ git push --tags
# Done.
	@ echo "Done."
	@ echo "If happy, please type:"
	@ echo "  \"make publish\"   to publish a new opam package"
	@ echo "  \"make export\"    to upload the documentation to yquem.inria.fr"

.PHONY: publish
publish:
# Publish an opam description.
	@ opam publish -v $(DATE) $(THIS) $(ARCHIVE) .

.PHONY: undo
undo:
# Undo the last release (assuming it was done on the same date).
	@ git tag -d $(DATE)
	@ git push -u origin :$(DATE)
