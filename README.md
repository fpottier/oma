# Oma

Oma is an OCaml library. It offers an implementation of the
order maintenance data structure described in
[this paper](https://erikdemaine.org/papers/DietzSleator_ESA2002/paper.pdf).

Here is its [documentation](http://cambium.inria.fr/~fpottier/oma/doc/oma/Oma/index.html).
