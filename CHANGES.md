# CHANGES

## 2024/06/19

* New functions `invalidate_open_interval`
  and `invalidate_semi_open_interval`.

* Fix a serious bug in `Unsafe.first` and `Unsafe.last`,
  which would incorrectly return `None`
  when the region contains only one point.

* Fix a serious bug in `Unsafe.iter`,
  which would systematically omit the last point of the region.

## 2024/01/06

* Initial release.
